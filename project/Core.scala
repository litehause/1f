import sbt._
import sbt.Keys.{libraryDependencies, _}

object Core {

  lazy val settings = Project.defaultSettings ++ Seq(
    organization := "ru.onef",

    version      := "1.0-SNAPSHOT",

    scalaVersion := "2.11.8",

    scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked"),

    javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
  )

  lazy val databaseSettings = Seq(
    libraryDependencies += "com.typesafe.slick" % "slick_2.11" % "2.1.0",

    libraryDependencies += "org.postgresql" % "postgresql" % "9.3-1102-jdbc41",

    libraryDependencies += "com.zaxxer" % "HikariCP" % "2.3.2"
  )  
  
}
