import sbt._
import Keys._
import sbtassembly.Plugin.{AssemblyKeys, MergeStrategy, PathList}
import AssemblyKeys._ // put this at the top of the file


object WebBundleBuild extends Build {
  override lazy val settings = super.settings ++ XitrumPackage.skip

  lazy val core = Project(
    id = "core",
    base = file("core"),


    settings = Xitrum.settings ++ Xitrum.templateSettings ++ Core.databaseSettings ++ Seq(
      name := "core"
    ) ++ XitrumPackage.skip
  )

  lazy val rest = Project(
    id = "rest",
    base = file("rest"),
    settings =
      Xitrum.settings ++
      Xitrum.templateSettings ++
      Seq(
        name := "rest",
        libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.4" % "test"
      ) ++
      XitrumPackage.copy("script", "config", "public", "script")
  ).dependsOn(core)

  lazy val dbCreator = Project(
    id = "db-creator",
    base = file("db-creator"),
    settings =
      Core.settings ++
        sbtassembly.Plugin.assemblySettings ++
        Seq(name := "db-creator",
          libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.5",
          mainClass := Some("ru.onef.dbcreator.Main"),
          jarName := "dbcreator.jar",
          mergeStrategy in assembly := {
            case PathList("META-INF", xs @ _*) => MergeStrategy.discard
            case x => MergeStrategy.first
          }
        )
  ).dependsOn(core)
}
