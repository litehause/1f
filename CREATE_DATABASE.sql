CREATE DATABASE one_factor;
CREATE USER pg_one_factor WITH password '123456';
GRANT ALL ON DATABASE one_factor TO pg_one_factor;

--- psql -h localhost one_factor pg_one_factor
CREATE TABLE user_label (
  user_id bigint primary key,
  lon float NOT NULL,
  lat float NOT NULL
);


-- lat 90  max
-- lon 180 max
-- INSERT INTO user_label(lon, lat) VALUES (179.296875, 65.07213);


CREATE TABLE geographic_grid (
  tile_x INT NOT NULL,
  tile_y INT NOT NULL,
  distance_error INT NOT NULL,
  PRIMARY KEY (tile_x, tile_y)
);

---INSERT INTO geographic_grid(tile_x, tile_y, distance_error) VALUES ();