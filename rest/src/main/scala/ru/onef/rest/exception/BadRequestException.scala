package ru.onef.rest.exception

case class BadRequestException(message: Option[String]) extends Exception
