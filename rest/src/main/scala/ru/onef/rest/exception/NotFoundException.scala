package ru.onef.rest.exception

case class NotFoundException(message: Option[String]) extends Exception
