package ru.onef.rest.action

import ru.onef.core.db.DBConnection
import ru.onef.core.model.{GeographicGrid, UserLabel}
import ru.onef.rest.exception.NotFoundException
import xitrum.annotation.{GET, Swagger}

@GET("/count/user/in/grid")
@Swagger(
  Swagger.Tags("count user in grid"),
  Swagger.Description("count user in grid"),
  Swagger.FloatQuery("lon", "longitude"),
  Swagger.FloatQuery("lat", "latitude")
)
class CountUserInGrid extends AbstractRestAction with DBConnection {
  override protected def calculateResponse(): Any = {
    val lon = param[Float]("lon")
    val lat = param[Float]("lat")
    db withSession { implicit session =>
      val grid = GeographicGrid.find(lon.toInt, lat.toInt)
          .getOrElse(throw NotFoundException(Some(s"grid with location lon $lon and lat $lat not found")))
      UserLabel.countInLocation(grid.tileX, grid.tileY, grid.tileX + 1, grid.tileY + 1)
    }



  }
}
