package ru.onef.rest.action

import io.netty.handler.codec.http.HttpResponseStatus
import ru.onef.rest.exception.{BadRequestException, NotFoundException}
import xitrum.Action

case class Response(status: Int = HttpResponseStatus.OK.code(),
                    data: Option[Any] = None,
                    errorMessage: Option[String] = None)

abstract class AbstractRestAction extends Action {
  override def execute(): Unit = {
    try {
      val data = calculateResponse()
      respondJson(Response(data = Some(data)))
    } catch {
      case e: BadRequestException =>
        response.setStatus(HttpResponseStatus.BAD_REQUEST)
        respondJson(Response(status = HttpResponseStatus.BAD_REQUEST.code(), errorMessage = e.message))
      case e:NotFoundException =>
        response.setStatus(HttpResponseStatus.NOT_FOUND)
        respondJson(Response(status = HttpResponseStatus.NOT_FOUND.code(), errorMessage = e.message))
      case e: Exception =>
        log.error(e.getMessage, e)
        response.setStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR)
        respondJson(Response(status = HttpResponseStatus.INTERNAL_SERVER_ERROR.code()))
    }
  }

  protected def calculateResponse(): Any
}
