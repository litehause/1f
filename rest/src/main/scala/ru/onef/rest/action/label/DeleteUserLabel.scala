package ru.onef.rest.action.label

import ru.onef.core.db.DBConnection
import ru.onef.core.model.UserLabel
import ru.onef.rest.action.AbstractRestAction
import ru.onef.rest.exception.NotFoundException
import xitrum.annotation.{DELETE, Swagger}

@DELETE("/user/label/delete")
@Swagger(
  Swagger.Description("delete user label"),
  Swagger.LongQuery("userId", "User Id"),
  Swagger.Response(200, "success delete user")
)
class DeleteUserLabel extends UserLabelApi with DBConnection {
  override protected def calculateResponse(): Any = {
    val userId = param[Long]("userId")

    db withTransaction { implicit session =>
      UserLabel.findById(userId)
        .getOrElse(throw NotFoundException(Some(s"user with id: $userId Not found")))
      UserLabel.delete(userId)
      "ok"
    }
  }
}
