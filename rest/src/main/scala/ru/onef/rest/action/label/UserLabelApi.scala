package ru.onef.rest.action.label

import ru.onef.rest.action.AbstractRestAction
import xitrum.SkipCsrfCheck
import xitrum.annotation.Swagger

@Swagger(
  Swagger.Tags("API user labels")
)
abstract class UserLabelApi extends AbstractRestAction with SkipCsrfCheck{

}
