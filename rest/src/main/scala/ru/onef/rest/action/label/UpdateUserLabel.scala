package ru.onef.rest.action.label

import ru.onef.core.db.DBConnection
import ru.onef.core.model.UserLabel
import ru.onef.rest.action.AbstractRestAction
import ru.onef.rest.exception.NotFoundException
import xitrum.annotation.{PUT, Swagger}


@PUT("/user/label/update")
@Swagger(
  Swagger.Description("update user label"),
  Swagger.LongForm("userId", "User Id"),
  Swagger.FloatForm("lon", "longitude"),
  Swagger.FloatForm("lat", "latitude"),
  Swagger.Response(200, "success update user")
)
class UpdateUserLabel extends UserLabelApi with ValidateLocation with DBConnection {

  override protected def calculateResponse(): Any = {
    val userId = param[Long]("userId")
    val lon = param[Float]("lon")
    val lat = param[Float]("lat")
    validateLocation(lon, lat)
    db withTransaction { implicit session =>
      UserLabel
        .update(userId = userId, lon = lon, lat = lat)
        .getOrElse(throw NotFoundException(Some(s"user with id: $userId Not found")))
    }
  }
}
