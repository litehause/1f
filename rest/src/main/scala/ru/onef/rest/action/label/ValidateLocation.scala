package ru.onef.rest.action.label

import ru.onef.rest.exception.BadRequestException


trait ValidateLocation {

  private def validateLon(lon: Float): Boolean = {
    val absLon = Math.abs(lon)
    absLon match {
      case lon if lon >= 0 && lon <=180 => true
      case _ => false
    }
  }

  private def validateLat(lat: Float): Boolean = {
    val absLat = Math.abs(lat)
    absLat match {
      case lon if lon >= 0 && lon <= 90 => true
      case _ => false
    }
  }

  def validateLocation(lon: Float, lat: Float) = {
    if (!(validateLat(lat) && validateLon(lon))) {
      throw new BadRequestException(Some("invalide validate coordinate"))
    }
  }
}
