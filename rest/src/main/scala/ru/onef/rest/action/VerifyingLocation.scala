package ru.onef.rest.action

import ru.onef.core.db.DBConnection
import ru.onef.core.model.{GeographicGrid, UserLabel}
import ru.onef.rest.exception.NotFoundException
import ru.onef.rest.service.GeoService
import xitrum.annotation.{GET, Swagger}


case class VerifyingLocationResponse(moreThanDistanceError: Boolean)

@GET("verifying/location")
@Swagger(
  Swagger.Tags("verifying location"),
  Swagger.Description("verifying location"),
  Swagger.LongQuery("userId", "User Id"),
  Swagger.FloatQuery("lon", "longitude"),
  Swagger.FloatQuery("lat", "latitude")
)
class VerifyingLocation extends AbstractRestAction with DBConnection {
  override protected def calculateResponse(): Any = {
    val lon = param[Float]("lon")
    val lat = param[Float]("lat")
    val user = paramo[Long]("userId")
      .flatMap { userId =>
        db withSession { implicit session =>
          UserLabel.findById(userId)
        }
      }.getOrElse(throw NotFoundException(Some("User not found")))

    val georgaphicGrid = db withSession { implicit session =>
      GeographicGrid.find(user.lon.toInt, user.lat.toInt)
        .getOrElse(throw NotFoundException(Some(s"Geographic grid with location lon ${user.lon} and lat ${user.lat} Not Found")))
    }

    val distance = GeoService.getDistance(lon, lat, user.lon, user.lat)

    if (georgaphicGrid.distanceError - distance > 0) {
      VerifyingLocationResponse(true)
    } else {
      VerifyingLocationResponse(false)
    }
  }

}
