package ru.onef.rest.action.label

import ru.onef.core.db.DBConnection
import ru.onef.core.model.UserLabel
import ru.onef.rest.action.AbstractRestAction
import ru.onef.rest.exception.BadRequestException
import xitrum.SkipCsrfCheck
import xitrum.annotation.{POST, Swagger}

@POST("/user/label/add")
@Swagger(
  Swagger.Description("add user label"),
  Swagger.LongForm("userId", "User Id"),
  Swagger.FloatForm("lon", "longitude"),
  Swagger.FloatForm("lat", "latitude"),
  Swagger.Response(200, "success create user")
)
class AddUserLabel extends UserLabelApi with ValidateLocation with DBConnection {
  override protected def calculateResponse(): Any = {
    val userId = param[Long]("userId")
    val lon = param[Float]("lon")
    val lat = param[Float]("lat")
    validateLocation(lon, lat)
    db withTransaction { implicit session =>
      if (UserLabel.findById(userId).isDefined) {
        throw BadRequestException(Some("A user with this id exists"))
      }
      UserLabel.add(UserLabel(userId = userId, lon = lon, lat = lat))
    }
  }
}
