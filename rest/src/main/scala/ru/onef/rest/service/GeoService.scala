package ru.onef.rest.service

object GeoService {


  def getDistance(lon1: Float, lat1: Float, lon2: Float, lat2: Float):Int = {
    val mlon = Math.abs(lon1 - lon2)
    val mlat = Math.abs(lat1 - lat2)
    (111.2 *
      Math.sqrt(Math.pow(lon1 - lon2, 2) +
        (lat1 - lat2) *
          Math.cos(Math.PI * lon1 / 180) *
          (lat1 - lat2) * Math.cos(Math.PI * lon1 / 180)) * 1000).toInt
  }


}
