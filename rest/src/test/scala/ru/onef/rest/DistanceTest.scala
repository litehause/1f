package ru.onef.rest

import collection.mutable.Stack
import org.scalatest._
import ru.onef.rest.service.GeoService

class DistanceTest extends FlatSpec with Matchers {

  //last max 180
  //53.877239,86.607671

  //53.877213,86.608787

  it should "test distance" in {
    GeoService.getDistance(53.877302F, 86.606791F, 53.877315F,86.609452F) should be(174)
  }
}
