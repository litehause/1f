package ru.onef.core.model

import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.JdbcBackend.Database._
import scala.slick.jdbc.JdbcBackend.Session

case class GeographicGrid(tileX: Int, //lon
                          tileY: Int, //lat
                          distanceError: Int)

object GeographicGrid {
  implicit val getResult = GetResult(r => GeographicGrid(r.<<, r.<<, r.<<))


  def add(geographicGrid: GeographicGrid)(implicit session: Session): GeographicGrid = {
    sql"""
      INSERT INTO geographic_grid(tile_x, tile_y, distance_error)
      VALUES (${geographicGrid.tileX}, ${geographicGrid.tileY}, ${geographicGrid.distanceError})
      returning tile_x, tile_y, distance_error
    """.as[GeographicGrid].first(session)
  }

  def find(tileX: Int, tileY: Int)(implicit session: Session): Option[GeographicGrid] = {
    sql"""
      SELECT tile_x, tile_y, distance_error
      FROM geographic_grid
      WHERE tile_x = $tileX AND tile_y = $tileY
    """.as[GeographicGrid].firstOption(session)
  }
}
