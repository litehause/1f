package ru.onef.core.model

import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.JdbcBackend.Database._
import scala.slick.jdbc.JdbcBackend.Session

case class UserLabel(userId: Long, lon: Float, lat: Float)

object UserLabel {

  implicit val getResult = GetResult(r => UserLabel(r.<<, r.<<, r.<<))


  def add(userLabel: UserLabel)(implicit session: Session): UserLabel = {
    sql"""
     INSERT INTO user_label(user_id, lon, lat)
     VALUES (${userLabel.userId}, ${userLabel.lon}, ${userLabel.lat})
     returning user_id, lon, lat
    """.as[UserLabel].first(session)
  }


  def findById(userId: Long)(implicit session: Session): Option[UserLabel] = {
    sql"""
      SELECT user_id, lon, lat
      FROM user_label
      WHERE user_id = $userId
    """.as[UserLabel].firstOption(session)
  }

  def update(userId: Long, lon: Float, lat: Float)(implicit session: Session) : Option[UserLabel] = {
    sql"""
      UPDATE user_label
      SET lon = $lon, lat = $lat
      WHERE user_id = $userId
      returning user_id, lon, lat
    """.as[UserLabel].firstOption(session)
  }

  def delete(userId: Long)(implicit session: Session) = {
    sqlu"""
      DELETE FROM user_label
      WHERE user_id = $userId
    """.execute(session)
  }


  def countInLocation(minLon: Float, minLat: Float,
                      maxLon: Float, maxLat: Float)(implicit session: Session): Long = {
    sql"""
      select count(*)
      from user_label
      where lat BETWEEN $minLat and $maxLat AND
      lon BETWEEN $minLon and $maxLon
    """.as[Long].first(session)
  }

}
