package ru.onef.core.configuration

object DBConfiguration {

  import Configuration._

  lazy val user = config.getString("db.user")

  lazy val password = config.getString("db.password")

  lazy val dbName = config.getString("db.dbName")

  lazy val host = config.getString("db.host")

  val driverClassName = "org.postgresql.ds.PGSimpleDataSource"

  val maxPoolSize = 10

  val minimumIdle = 5
}