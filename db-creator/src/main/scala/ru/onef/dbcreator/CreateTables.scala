package ru.onef.dbcreator

import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.JdbcBackend.Database._
import scala.slick.jdbc.JdbcBackend.Session

object CreateTables {
  def create()(implicit session: Session) = {
    createUserLabelTable()(session)
    createGeographicGrid()(session)
  }

  private def createUserLabelTable()(implicit session: Session) = {
    sqlu"""
       CREATE TABLE IF NOT EXISTS user_label (
         user_id bigint primary key,
         lon float NOT NULL,
         lat float NOT NULL
       );
    """.execute(session)
  }


  private def createGeographicGrid()(implicit session: Session) = {
    sqlu"""
       CREATE TABLE IF NOT EXISTS geographic_grid   (
         tile_x INT NOT NULL,
         tile_y INT NOT NULL,
         distance_error INT NOT NULL,
         PRIMARY KEY (tile_x, tile_y)
       );
    """.execute(session)
  }
}
