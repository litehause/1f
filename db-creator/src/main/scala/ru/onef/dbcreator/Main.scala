package ru.onef.dbcreator

import java.io.File

import com.github.tototoshi.csv.{CSVReader, CSVWriter}
import ru.onef.core.db.DBConnection
import ru.onef.core.model.{GeographicGrid, UserLabel}

import scala.util.Random

object Main extends DBConnection {

  def main(args: Array[String]): Unit = {
    db withTransaction { implicit session =>
      CreateTables.create()
    }
    saveUserLabel(args(0))
    saveGeographicGridFromCSV(args(1))
  }

  private def saveUserLabel(csvPath: String) = {
    val userLabelsFile = new File(csvPath)
    val userLabelsReader = CSVReader.open(userLabelsFile)
    db withTransaction { implicit session =>
      for (userLabelCsvRow <- userLabelsReader.iterator) {
        val userLabel = UserLabel(
          userId = userLabelCsvRow.head.toLong,
          lon = userLabelCsvRow(1).toFloat,
          lat = userLabelCsvRow(2).toFloat)
        UserLabel.add(userLabel)
      }
    }
  }

  private def saveGeographicGridFromCSV(csvPath: String) = {
    val geographicGridFile = new File(csvPath)
    val reader = CSVReader.open(geographicGridFile)
    db withTransaction { implicit session =>
      for (geographicGridCSVRow <- reader.iterator) {
        val geographicGrid = GeographicGrid(
          tileX = geographicGridCSVRow.head.toInt,
          tileY = geographicGridCSVRow(1).toInt,
          distanceError = geographicGridCSVRow(2).toInt
        )
        GeographicGrid.add(geographicGrid)
      }
    }
  }
}
